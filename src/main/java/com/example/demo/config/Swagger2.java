package com.example.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Predicate;

import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author caowenjun
 * @date 2019/02/25
 */
@Configuration
@EnableSwagger2
public class Swagger2 {

	/**
	 * 默认值为空
	 */
	@Value("${server.host:}")
	private String serverHost;

	@Bean
	public Docket createRestApi() {
		Predicate<RequestHandler> predicate = input -> {
			Class<?> declaringClass = input.declaringClass();
			// 被注解的类
			if (declaringClass.isAnnotationPresent(RestController.class)) {
				return true;
			}
			// 被注解的方法
			if (input.isAnnotatedWith(Controller.class)) {
				return true;
			}
			return false;
		};

		return new Docket(DocumentationType.SWAGGER_2)
				.host(serverHost)
				.apiInfo(apiInfo())
				.select()
				.apis(predicate)
				.paths(PathSelectors.any())
				.build();
	}

	//private String description = "### 测试接口文档";

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("测试接口文档")
				//.description(description)
				//.termsOfServiceUrl("www.baidu.com")
				//.contact(new Contact("Caowenjun", "http://www.baidu.com", "496190712@qq.com"))
				.version("1.0").build();
	}

}