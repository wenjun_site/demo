package com.example.demo.web.controller;

import com.example.demo.web.domain.WxUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.web.service.DemoService;
import com.github.pagehelper.PageInfo;

@RestController
@RequestMapping("demo")
public class DemoController {

	@Autowired
	private DemoService demoService;

	@GetMapping("/id/{id}")
	public WxUser getUserById(@PathVariable("id") Integer id) {
		return demoService.getUserById(id);
	}

	@GetMapping("/name/{name}")
	public WxUser getUserByName(@PathVariable("name") String name) {
		return demoService.getUserByName(name);
	}

	@GetMapping()
	public PageInfo<WxUser> getUserPage() {
		return demoService.getUserPage();
	}

	@PostMapping
	public String addUser(@RequestBody WxUser user) {
		return demoService.addUser(user);
	}

	@PostMapping("addMaster")
	public String addMaster(@RequestBody WxUser master) {
		return demoService.addMaster(master);
	}
}
