package com.example.demo.web.controller;

import com.example.demo.web.domain.VRecord;
import com.example.demo.web.domain.WxUser;
import com.example.demo.web.domain.form.LoginForm;
import com.example.demo.web.domain.model.AjaxResult;
import com.example.demo.web.service.AppService;
import com.example.demo.web.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 小程序Controller
 *
 * @author cnwejun
 * @date 2021-11-24
 */
@Api(tags = "App端接口集合")
@RestController
@RequestMapping("/app")
public class AppController {

    @Autowired
    private AppService appService;

    @Autowired
    private UserService userService;

    @ApiOperation("获取床位列表")
    @GetMapping("/bedList")
    public AjaxResult bedList(Long userId) {
        List<VRecord> records = appService.bedList();
        AjaxResult result = AjaxResult.success(records);
        if (userId != null) {
            WxUser user = userService.selectUserByuserId(userId);
            if (user != null) {
                result.put("isAuth", user.getIsAuth());
            }
        }
        return result;
    }

    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginForm form) {
        return appService.login(form);
    }

    @PostMapping("/saveOrUpdate")
    public AjaxResult saveOrUpdate(@RequestBody VRecord record) {
        return appService.saveOrUpdate(record);
    }

    @GetMapping("/outHospital")
    public AjaxResult outHospital(Long bedNo) {
        return appService.outHospital(bedNo);
    }
}
