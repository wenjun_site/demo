package com.example.demo.web.mapper;

import com.example.demo.web.common.DemoBaseMapper;
import com.example.demo.web.domain.VRecord;

public interface VRecordMapper extends DemoBaseMapper<VRecord> {
}