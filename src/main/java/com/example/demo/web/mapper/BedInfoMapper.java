package com.example.demo.web.mapper;

import com.example.demo.web.common.DemoBaseMapper;
import com.example.demo.web.domain.BedInfo;

public interface BedInfoMapper extends DemoBaseMapper<BedInfo> {
}