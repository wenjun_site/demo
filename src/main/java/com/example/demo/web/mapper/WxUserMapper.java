package com.example.demo.web.mapper;

import com.example.demo.web.common.DemoBaseMapper;
import com.example.demo.web.domain.WxUser;

public interface WxUserMapper extends DemoBaseMapper<WxUser> {
}