package com.example.demo.web.mapper;

import com.example.demo.web.common.DemoBaseMapper;
import com.example.demo.web.domain.BedRecord;

public interface BedRecordMapper extends DemoBaseMapper<BedRecord> {
}