package com.example.demo.web.service;

import com.example.demo.web.domain.VRecord;
import com.example.demo.web.domain.form.LoginForm;
import com.example.demo.web.domain.model.AjaxResult;

import java.util.List;

public interface AppService {

    /**
     * @return
     */
    List<VRecord> bedList();

    /**
     * 微信登录
     *
     * @param form
     * @return
     */
    AjaxResult login(LoginForm form);

    AjaxResult saveOrUpdate(VRecord record);

    /**
     * 出院
     *
     * @param bedNo
     * @return
     */
    AjaxResult outHospital(Long bedNo);


}
