package com.example.demo.web.service.impl;


import com.example.demo.web.domain.WxUser;
import com.example.demo.web.mapper.WxUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.example.demo.web.service.DemoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Slf4j
@Service
public class DemoServiceImpl implements DemoService {

    @Autowired
    private WxUserMapper userMapper;

    @Override
    public PageInfo<WxUser> getUserPage() {
        log.info("分页查询开始。。。");
        Example example = new Example(WxUser.class);
        Example.Criteria criteria = example.createCriteria();
        //criteria.andLike(, "");
        example.orderBy("patientId").asc();
        PageHelper.startPage(0, 10);
        List<WxUser> uts = userMapper.selectByExample(example);
        PageInfo<WxUser> pageInfo = new PageInfo<>(uts);
        return pageInfo;
    }

    @Override
    public String addMaster(WxUser master) {
        int count = userMapper.insert(master);
        if (count > 0) {
            return JSON.toJSONString(master);
        }
        return "新增失败";
    }

    @Override
    public String addUser(WxUser user) {
        int count = userMapper.insert(user);
        if (count > 0) {
            return JSON.toJSONString(user);
        }
        return "新增失败";
    }

    @Override
    public WxUser getUserById(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public WxUser getUserByName(String name) {
        return userMapper.selectAll().get(0);
    }
}
