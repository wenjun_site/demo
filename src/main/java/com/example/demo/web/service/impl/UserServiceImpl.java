package com.example.demo.web.service.impl;


import com.example.demo.util.Tools;
import com.example.demo.web.domain.WxUser;
import com.example.demo.web.mapper.WxUserMapper;
import com.example.demo.web.service.UserService;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private WxUserMapper userMapper;


    @Override
    public WxUser selectUserByOpenId(String openId) {
        Example example = new Example(WxUser.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo(WxUser.OPENID, openId);
        criteria.andEqualTo(WxUser.DEL_FLAG, false);
        List<WxUser> wxUsers = userMapper.selectByExample(example);
        if (Tools.listNotEmpty(wxUsers) && wxUsers.size() == 1) {
            return wxUsers.get(0);
        }
        log.info("openId为{}的数据查询到0条或者多条", openId);
        return null;
    }

    @Override
    public int updateWxUser(WxUser wxUser) {
        return userMapper.updateByPrimaryKeySelective(wxUser);
    }

    @Override
    public int insertWxUser(WxUser wxUser) {
        wxUser.setDelFlag(false);
        wxUser.setCreateBy("miniapp");
        wxUser.setCreateTime(new Date());
        return userMapper.insert(wxUser);
    }

    @Override
    public WxUser selectUserByuserId(Long userId) {
        return userMapper.selectByPrimaryKey(userId);
    }
}
