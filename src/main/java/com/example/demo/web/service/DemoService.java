package com.example.demo.web.service;


import com.example.demo.web.domain.WxUser;
import com.github.pagehelper.PageInfo;

public interface DemoService {

	PageInfo<WxUser> getUserPage();

	WxUser getUserById(Integer id);

	String addUser(WxUser user);

	WxUser getUserByName(String name);

	String addMaster(WxUser master);
}
