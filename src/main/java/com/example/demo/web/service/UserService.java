package com.example.demo.web.service;

import com.example.demo.web.domain.WxUser;

public interface UserService {

    /**
     * 根据openid查询用户
     *
     * @param openId
     * @return
     */
    WxUser selectUserByOpenId(String openId);

    int updateWxUser(WxUser wxUser);

    int insertWxUser(WxUser wxUser);

    WxUser selectUserByuserId(Long userId);
}
