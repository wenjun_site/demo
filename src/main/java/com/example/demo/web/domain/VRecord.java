package com.example.demo.web.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
@Table(name = "`v_record`")
public class VRecord implements Serializable {
    /**
     * 流水号
     */
    @Column(name = "`bed_no`")
    private Long bedNo;

    /**
     * 床位名称
     */
    @Column(name = "`bed_name`")
    private String bedName;

    /**
     * 展示状态
     */
    @Column(name = "`is_show`")
    private Boolean isShow;

    /**
     * 删除标志
     */
    @Column(name = "`del_flag`")
    private Boolean delFlag;

    /**
     * 流水号
     */
    @Column(name = "`id`")
    private Long id;

    /**
     * 患者姓名
     */
    @Column(name = "`patient_name`")
    private String patientName;

    /**
     * 住院时间
     */
    @Column(name = "`in_date`")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date inDate;

    /**
     * 出院时间
     */
    @Column(name = "`out_date`")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date outDate;

    @Column(name = "`in_days`")
    private Integer inDays;

    @Column(name = "`group_no`")
    private Long groupNo;

    /**
     * 在院状态
     */
    @Column(name = "`state`")
    private Boolean state;

    /**
     * 创建时间
     */
    @Column(name = "`create_time`")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "`update_time`")
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public static final String BED_NO = "bedNo";

    public static final String BED_NAME = "bedName";

    public static final String IS_SHOW = "isShow";

    public static final String DEL_FLAG = "delFlag";

    public static final String ID = "id";

    public static final String PATIENT_NAME = "patientName";

    public static final String IN_DATE = "inDate";

    public static final String OUT_DATE = "outDate";

    public static final String IN_DAYS = "inDays";

    public static final String GROUP_NO = "groupNo";

    public static final String STATE = "state";

    public static final String CREATE_TIME = "createTime";

    public static final String UPDATE_TIME = "updateTime";

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", bedNo=").append(bedNo);
        sb.append(", bedName=").append(bedName);
        sb.append(", isShow=").append(isShow);
        sb.append(", delFlag=").append(delFlag);
        sb.append(", id=").append(id);
        sb.append(", patientName=").append(patientName);
        sb.append(", inDate=").append(inDate);
        sb.append(", outDate=").append(outDate);
        sb.append(", inDays=").append(inDays);
        sb.append(", groupNo=").append(groupNo);
        sb.append(", state=").append(state);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}