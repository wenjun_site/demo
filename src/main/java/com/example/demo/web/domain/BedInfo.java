package com.example.demo.web.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "`bed_info`")
public class BedInfo implements Serializable {
    /**
     * 流水号
     */
    @Id
    @Column(name = "`bed_no`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bedNo;

    /**
     * 床位名称
     */
    @Column(name = "`bed_name`")
    private String bedName;

    /**
     * 展示状态
     */
    @Column(name = "`is_show`")
    private Boolean isShow;

    /**
     * 删除标志
     */
    @Column(name = "`del_flag`")
    private Boolean delFlag;

    /**
     * 创建时间
     */
    @Column(name = "`create_time`")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "`update_time`")
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public static final String BED_NO = "bedNo";

    public static final String BED_NAME = "bedName";

    public static final String IS_SHOW = "isShow";

    public static final String DEL_FLAG = "delFlag";

    public static final String CREATE_TIME = "createTime";

    public static final String UPDATE_TIME = "updateTime";

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", bedNo=").append(bedNo);
        sb.append(", bedName=").append(bedName);
        sb.append(", isShow=").append(isShow);
        sb.append(", delFlag=").append(delFlag);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}