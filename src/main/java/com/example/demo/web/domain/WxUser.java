package com.example.demo.web.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "`wx_user`")
public class WxUser implements Serializable {
    /**
     * 流水号
     */
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 手机号码
     */
    @Column(name = "`mobile`")
    private String mobile;

    /**
     * 微信昵称
     */
    @Column(name = "`nick_name`")
    private String nickName;

    /**
     * 微信头像
     */
    @Column(name = "`avatar_url`")
    private String avatarUrl;

    /**
     * 性别0男1女
     */
    @Column(name = "`gender`")
    private String gender;

    /**
     * 国家
     */
    @Column(name = "`country`")
    private String country;

    /**
     * 省份
     */
    @Column(name = "`province`")
    private String province;

    /**
     * 城市
     */
    @Column(name = "`city`")
    private String city;

    /**
     * 语言
     */
    @Column(name = "`language`")
    private String language;

    /**
     * 类型1医生2管理
     */
    @Column(name = "`type`")
    private String type;

    /**
     * 授权
     */
    @Column(name = "`is_auth`")
    private Boolean isAuth;

    /**
     * openid
     */
    @Column(name = "`openid`")
    private String openid;

    /**
     * sessionKey
     */
    @Column(name = "`session_key`")
    private String sessionKey;

    /**
     * 删除标志
     */
    @Column(name = "`del_flag`")
    private Boolean delFlag;

    /**
     * 创建者
     */
    @Column(name = "`create_by`")
    private String createBy;

    /**
     * 创建时间
     */
    @Column(name = "`create_time`")
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public static final String ID = "id";

    public static final String MOBILE = "mobile";

    public static final String NICK_NAME = "nickName";

    public static final String AVATAR_URL = "avatarUrl";

    public static final String GENDER = "gender";

    public static final String COUNTRY = "country";

    public static final String PROVINCE = "province";

    public static final String CITY = "city";

    public static final String LANGUAGE = "language";

    public static final String TYPE = "type";

    public static final String IS_AUTH = "isAuth";

    public static final String OPENID = "openid";

    public static final String SESSION_KEY = "sessionKey";

    public static final String DEL_FLAG = "delFlag";

    public static final String CREATE_BY = "createBy";

    public static final String CREATE_TIME = "createTime";

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", mobile=").append(mobile);
        sb.append(", nickName=").append(nickName);
        sb.append(", avatarUrl=").append(avatarUrl);
        sb.append(", gender=").append(gender);
        sb.append(", country=").append(country);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", language=").append(language);
        sb.append(", type=").append(type);
        sb.append(", isAuth=").append(isAuth);
        sb.append(", openid=").append(openid);
        sb.append(", sessionKey=").append(sessionKey);
        sb.append(", delFlag=").append(delFlag);
        sb.append(", createBy=").append(createBy);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}