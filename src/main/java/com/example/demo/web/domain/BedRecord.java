package com.example.demo.web.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "`bed_record`")
public class BedRecord implements Serializable {
    /**
     * 流水号
     */
    @Id
    @Column(name = "`id`")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 床位号
     */
    @Column(name = "`bed_no`")
    private Long bedNo;

    /**
     * 患者姓名
     */
    @Column(name = "`patient_name`")
    private String patientName;

    /**
     * 住院时间
     */
    @Column(name = "`in_date`")
    private Date inDate;

    /**
     * 分组号
     */
    @Column(name = "`group_no`")
    private Integer groupNo;

    /**
     * 在院状态
     */
    @Column(name = "`state`")
    private Boolean state;

    /**
     * 出院时间
     */
    @Column(name = "`out_date`")
    private Date outDate;

    /**
     * 删除标志
     */
    @Column(name = "`del_flag`")
    private Boolean delFlag;

    /**
     * 创建时间
     */
    @Column(name = "`create_time`")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "`update_time`")
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public static final String ID = "id";

    public static final String BED_NO = "bedNo";

    public static final String PATIENT_NAME = "patientName";

    public static final String IN_DATE = "inDate";

    public static final String GROUP_NO = "groupNo";

    public static final String STATE = "state";

    public static final String OUT_DATE = "outDate";

    public static final String DEL_FLAG = "delFlag";

    public static final String CREATE_TIME = "createTime";

    public static final String UPDATE_TIME = "updateTime";

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", bedNo=").append(bedNo);
        sb.append(", patientName=").append(patientName);
        sb.append(", inDate=").append(inDate);
        sb.append(", groupNo=").append(groupNo);
        sb.append(", state=").append(state);
        sb.append(", outDate=").append(outDate);
        sb.append(", delFlag=").append(delFlag);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}