package com.example.demo.web.domain.form;

import lombok.Data;

@Data
public class LoginForm {

    // 小程序appId
    private String appId;

    // 0 H5 App,1 Wx
    private Long id;

    // 0 H5 App,1 Wx
    private String type;

    // wx登录code
    private String code;

    // app登录成功缓存的uuid
    private String uuid;

    // mobile
    private String mobile;

    // captcha
    private String captcha;

    // 邀请码 渠道
    private String scene;

    // 头像
    private String avatarUrl;

    // 城市
    private String city;

    // 国家
    private String country;

    // 性别
    private String gender;

    // 昵称
    private String nickName;

    // 省份
    private String province;

    private String language;

    // iv
    private String iv;

    // encryptedData
    private String encryptedData;
}
