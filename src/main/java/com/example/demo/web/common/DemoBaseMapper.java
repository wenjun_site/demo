package com.example.demo.web.common;

import tk.mybatis.mapper.common.BaseMapper;
import tk.mybatis.mapper.common.ExampleMapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author caowenjun
 *
 */
public interface DemoBaseMapper<T> extends BaseMapper<T>, ExampleMapper<T>, MySqlMapper<T> {

}
