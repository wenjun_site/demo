/*
 Navicat MySQL Data Transfer

 Source Server         : bestavi
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : bestavi:3306
 Source Schema         : bedmap

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 25/11/2021 11:04:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bed_info
-- ----------------------------
DROP TABLE IF EXISTS `bed_info`;
CREATE TABLE `bed_info`  (
  `bed_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '流水号',
  `bed_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '床位名称',
  `is_show` tinyint(1) NOT NULL COMMENT '展示状态',
  `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标志',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`bed_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '床位信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bed_record
-- ----------------------------
DROP TABLE IF EXISTS `bed_record`;
CREATE TABLE `bed_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '流水号',
  `bed_no` bigint(20) NOT NULL COMMENT '床位号',
  `patient_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '患者姓名',
  `in_date` datetime(0) NOT NULL COMMENT '住院时间',
  `group_no` int(11) NOT NULL COMMENT '分组号',
  `state` tinyint(1) NOT NULL COMMENT '在院状态',
  `out_date` datetime(0) NOT NULL COMMENT '出院时间',
  `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标志',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wx_user
-- ----------------------------
DROP TABLE IF EXISTS `wx_user`;
CREATE TABLE `wx_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '流水号',
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '微信昵称',
  `avatar_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '微信头像',
  `gender` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '性别0男1女',
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '国家',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省份',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '城市',
  `language` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '语言',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型1医生2管理',
  `is_auth` tinyint(1) NOT NULL COMMENT '授权',
  `openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'openid',
  `session_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'sessionKey',
  `del_flag` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标志',
  `create_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建者',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '微信用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- View structure for v_record
-- ----------------------------
DROP VIEW IF EXISTS `v_record`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_record` AS select `bi`.`bed_no` AS `bed_no`,`bi`.`bed_name` AS `bed_name`,`bi`.`is_show` AS `is_show`,`bi`.`del_flag` AS `del_flag`,`br`.`id` AS `id`,`br`.`patient_name` AS `patient_name`,`br`.`in_date` AS `in_date`,`br`.`out_date` AS `out_date`,(to_days(now()) - to_days(`br`.`in_date`)) AS `in_days`,ifnull(`br`.`group_no`,0) AS `group_no`,`br`.`state` AS `state`,`br`.`create_time` AS `create_time`,`br`.`update_time` AS `update_time` from (`bed_info` `bi` left join `bed_record` `br` on(((`br`.`bed_no` = `bi`.`bed_no`) and (`br`.`state` = 1))));

SET FOREIGN_KEY_CHECKS = 1;
